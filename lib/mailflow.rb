require 'mailflow'
require 'mailflow/client'
require 'mailflow/config'

module Mailflow
  class << self
    attr_reader :config

    def configure
      @config ||= Config.new
      yield config if block_given?
    end

    def send_message(&block)
      Mailflow::Client.instance.send_message(&block)
    end

    def send_raw_message(&block)
      Mailflow::Client.instance.send_raw_message(&block)
    end
  end
end