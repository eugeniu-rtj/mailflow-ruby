require 'moonrope_client'
require 'mailflow/message_scope'
require 'mailflow/send_message'
require 'mailflow/send_raw_message'

module Mailflow
  class Client
    attr_reader :host, :server_key

    def self.instance
      @instance ||= new(Mailflow.config.host, Mailflow.config.server_key)
    end

    def initialize(host, server_key)
      @host = host
      @server_key = server_key
    end

    def messages
      @messages ||= MessageScope.new(self)
    end

    def send_message(&block)
      send_message = SendMessage.new(self)
      block.call(send_message)
      send_message.send!
    end

    def send_raw_message(&block)
      send_raw_message = SendRawMessage.new(self)
      block.call(send_raw_message)
      send_raw_message.send!
    end

    def moonrope
      @moonrope ||= MoonropeClient::Connection.new(@host, headers: { 'X-Server-API-Key' => @server_key }, ssl: true)
    end
  end
end