require 'mailflow/error'
require 'mailflow/header_set'
require 'mailflow/attachment'

module Mailflow
  class Message
    attr_reader :id, :token, :headers, :attachments, :raw_message

    def self.find_with_scope(scope, id)
      api = scope.client.moonrope.messages.message(id: id.to_i, _expansions: scope.expansions)
      if api.success?
        Message.new(scope.client, api.data)
      elsif api.status == 'error' && api.data['code'] == 'MessageNotFound'
        raise MessageNotFound.new(id)
      else
        raise Error, "Couldn't load message from API (#{api.data})"
      end
    end

    def self.method_missing(name, *args, &block)
      if MessageScope.instance_methods(false).include?(name)
        Mailflow::Client.instance.messages.send(name, *args, &block)
      else
        super
      end
    end

    def initialize(client, attributes)
      @client = client
      @attributes = attributes
      @id = @attributes['id']
      @token = @attributes['token']
      @headers = HeaderSet.new(from_expansion(:headers))
      @attachments = from_expansion(:attachments).map { |a| Attachment.new(a) }
      @raw_message = Base64.decode64(from_expansion(:raw_message))
    end

    ATTRIBUTES = {
      status: [:status, :status],
      last_delivery_attempt: [:status, :last_delivery_attempt, :timestamp],
      held?: [:status, :held, :boolean],
      hold_expiry: [:status, :hold_expiry, :timestamp],
      rcpt_to: [:details, :rcpt_to],
      mail_from: [:details, :mail_from],
      subject: [:details, :subject],
      message_id: [:details, :message_id],
      timestamp: [:details, :timestamp, :timestamp],
      direction: [:details, :direction],
      size: [:details, :size],
      bounce?: [:details, :bounce, :boolean],
      bounce_for_id: [:details, :bounce],
      tag: [:details, :tag],
      received_with_ssl?: [:details, :received_with_ssl, :boolean],
      inspected?: [:inspection, :inspected, :boolean],
      spam?: [:inspection, :spam, :boolean],
      spam_score: [:inspection, :spam_score],
      threat?: [:inspection, :threat, :boolean],
      threat_details: [:inspection, :threat_details],
      plain_body: [:plain_body],
      html_body: [:html_body]
    }

    def method_missing(name, *args, &block)
      if mapping = ATTRIBUTES[name.to_sym]
        expansion, attribute, type = mapping
        value = from_expansion(expansion, attribute)
        case type
          when :timestamp
            value ? Time.at(value) : nil
          when :boolean
            value == 1
          else
            value
        end
      else
        super
      end
    end

    private

      def from_expansion(expansion, attribute = nil, loaded = false)
        if @attributes.key?(expansion.to_s) || loaded
          attribute ? @attributes[expansion.to_s][attribute.to_s] : @attributes[expansion.to_s]
        else
          load_expansions(expansion)
          from_expansion(expansion, attribute, true)
        end
      end

      def load_expansions(*names)
        api = @client.moonrope.messages.message(id: self.id, _expansions: names)
        if api.success?
          names.each do |expansion_name|
            if api.data.key?(expansion_name.to_s)
              @attributes[expansion_name.to_s] = api.data[expansion_name.to_s]
            end
          end
        else
          raise Mailflow::Error, "Couldn't load expansion data (#{names.join(', ')}) for message ID '#{self.id}'"
        end
      end
  end
end