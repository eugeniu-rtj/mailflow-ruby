require 'base64'
require 'mailflow/send_result'

module Mailflow
  class SendRawMessage
    attr_accessor :attributes

    def initialize(client)
      @client = client
      @attributes = {}
    end

    def send!
      api = @client.moonrope.request(:send, :raw, @attributes)
      handle_api_response(api)
    end

    def mail_from(address)
      @attributes[:mail_from] = address
    end

    def rcpt_to(*addresses)
      @attributes[:rcpt_to] ||= []
      @attributes[:rcpt_to] += addresses
    end

    def data(data)
      @attributes[:data] = Base64.encode64(data)
    end

    private

      def handle_api_response(api)
        if api.success?
          SendResult.new(@client, api.data)
        elsif api.status == 'error'
          raise SendError.new(api.data['code'], api.data['message'])
        else
          raise Error, "Couldn't send message"
        end
      end
  end
end
