module Mailflow
  class Config
    attr_accessor :host, :server_key

    def initialize
      @host = ENV['MAILFLOW_HOST']
      @server_key = ENV['MAILFLOW_KEY']
    end

    def host
      @host ||= raise(Error, "The host has not been configured. Please set it using the `Mailflow.configure` block or utilize the `MAILFLOW_HOST` environment variable.")
    end

    def server_key
      @server_key ||= raise(Error, "The server key has not been configured. Please set it using the `Mailflow.configure` block or make use of the `MAILFLOW_KEY` environment variable.")
    end
  end
end