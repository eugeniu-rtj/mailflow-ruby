require 'base64'
require 'mailflow/send_result'

module Mailflow
  class SendMessage
    attr_reader :client
    attr_accessor :attributes

    def initialize(client)
      @client = client
      initialize_attributes
    end

    def send!
      api = client.moonrope.request(:send, :message, attributes)
      return SendResult.new(client, api.data) if api.success?

      raise SendError.new(api.data['code'], api.data['message']) if api.status == 'error'

      raise Error, "API response unsuccessful: #{api.data}"
    end

    %i[from sender subject tag reply_to plain_body html_body].each do |method_name|
      define_method(method_name) do |value|
        attributes[method_name] = value
      end
    end

    %i[to cc bcc].each do |method_name|
      define_method(method_name) do |*addresses|
        add_addresses(method_name, addresses)
      end
    end

    def header(key, value)
      attributes[:headers][key.to_s] = value
    end

    def attach(filename, content_type, data)
      attributes[:attachments] << {
        name: filename,
        content_type: content_type,
        data: Base64.encode64(data)
      }
    end

    private

      def initialize_attributes
        @attributes = {
          to: [],
          cc: [],
          bcc: [],
          headers: {},
          attachments: []
        }
      end

      def add_addresses(type, addresses)
        sanitized_addresses = sanitize_addresses(addresses)
        attributes[type] += sanitized_addresses
      end

      def sanitize_addresses(addresses)
        # implement address sanitization logic here
        addresses
      end
  end
end
