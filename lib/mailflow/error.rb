module Mailflow
  class Error < StandardError; end

  class MessageNotFound < Error
    def initialize(id)
      @id = id
    end

    def message
      "No message was found that matches the provided ID. '#{@id}'"
    end

    alias to_s message
  end

  class SendError < Error
    def initialize(code, error_message)
      @code = code
      @error_message = error_message
    end

    attr_reader :code, :error_message

    def message
      "[#{@code}] #{@error_message}"
    end

    alias to_s message
  end
end