module Mailflow
  class SendResult
    attr_reader :message_id, :recipients

    def initialize(client, result)
      @client = client
      @message_id = result['message_id']
      @recipients = build_recipients(result['messages'])
    end

    def [](recipient)
      recipients[recipient.to_s.downcase]
    end

    def size
      recipients.size
    end

    private

      def build_recipients(messages)
        messages.each_with_object({}) do |(recipient, message_details), hash|
          hash[recipient.to_s.downcase] = Message.new(@client, message_details)
        end
      end
  end
end