module Mailflow
  class HeaderSet
    attr_reader :headers

    def initialize(headers)
      @headers = headers.transform_keys(&:to_s).transform_keys(&:downcase)
    end

    def [](name)
      headers[name.to_s.downcase]
    end

    def has_key?(key)
      headers.has_key?(key.to_s.downcase)
    end

    def method_missing(*args, &block)
      headers.send(*args, &block)
    end

    def respond_to_missing?(method_name, include_private = false)
      headers.respond_to?(method_name) || super
    end
  end
end