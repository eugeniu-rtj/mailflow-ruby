require 'mailflow/message'

module Mailflow
  class MessageScope
    attr_reader :client

    def initialize(client)
      @client = client
      @includes = []
    end

    def includes(*includes)
      @includes.concat(includes)
      self
    end

    def expansions
      return true if @includes.include?(:all)
      @includes.map(&:to_s)
    end

    def find_by_id(id)
      Message.find_with_scope(self, id)
    end
  end
end