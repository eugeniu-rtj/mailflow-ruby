require 'base64'

module Mailflow
  class Attachment
    attr_reader :filename, :content_type, :size, :hash, :data

    def initialize(attributes)
      @filename = attributes['filename']
      @content_type = attributes['content_type']
      @size = attributes['size']
      @hash = attributes['hash']
      @data = Base64.decode64(attributes['data'])
    end
  end
end