# MailFlow Ruby Library

This library offers a user-friendly interface for sending emails via the MailFlow service and tracking their progress as they are delivered.

## Install

Add the `mailflow` gem to your Gemfile and run `bundle install`.

```ruby
gem 'mailflow', '~> 0.0.2'
```

## Config

To configure the gem you should set the MAILFLOW_HOST and MAILFLOW_KEY environment variables
or configure it as demonstrated below:

```ruby
Mailflow.configure do |c|
  c.host = "domain.mailflow.cloud"
  c.server_key = "SERVER_KEY_TOKEN"
end
```
