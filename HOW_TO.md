
## Send Emails

To transmit a message via the API, simply follow the example provided below. There is no need to specify all the fields shown.

```ruby
Mailflow.send_message do |m|
  # This is the sender's address from which the message will be sent. 
  # Please ensure that the address you enter here is one of the domains configured for your mail server.
  m.from "Company Name <company@address.com>"

  # This is the address of the recipient or recipients who will receive the message. 
  # You can provide multiple email addresses as strings using this method, or you can call the to method 
  # multiple times to add additional recipients.
  
  m.to "Client Name <client@address.com>"
  m.cc "sales@address.com", "info@address.com"
  m.bcc "hidden@address.com"

  # This is the subject line of the message.
  m.subject "A new solution for sending emails!"

  # This is the content for the message. It is essential to provide at least one of these options, 
  # and we strongly recommend sending messages with both parts.

  m.html_body "<p>Html Message body!</p>"
  m.plain_body "Plain message body!"

  # This function attaches a file to the message. You should supply the file name, content type, 
  # and the attachment content.
  m.attach "invoice.pdf", "application/pdf", File.read("invoice.pdf")
  
  # This feature allows you to include custom headers in your outbound messages, 
  # and you can add as many of these as you need.
  m.header "X-Some-Header", "Some value goes here!"

  # This is the tag. Tagging allows you to categorize and generate reports based on the various types of emails you send.
  m.tag "welcome"
end
```

If any issues arise with the data you've provided, a Mailflow::SendError exception will be raised. This exception will contain a code and a message that you can use to identify and address the issue.
```ruby

begin
  # Your call to `send_message`
rescue Mailflow::SendError => e
  e.code #=> UnauthenticatedFromAddress
  e.error_message #=> The From address is not authorised to send mail from this server
end
```

The result of your send call will return a Mailflow::SendResult object. If desired, you can use this object to obtain additional information about the messages you've just sent.

```ruby
# This will provide the ID of the generated message.
result.message_id     #=> e6e67800-0000-5z3g-c6c6-8c52g3fs29f5@amrp.mailflow.cloud

# This will return the count of actual recipients who received this message.
result.recipients     #=> 4

# This feature enables you to access the message sent to a specific recipient. 
# It will return a Mailflow::Message object. 
# Refer to the information below for details about the data contained within this object.
message = result['support@address.com']
message.id            #=> 12345
message.token         #=> Daagcidh88XZ

# Additionally, you can access all the recipients as a hash.
result.recipients.each do |address, message|
  address             #=> "support@address.com"
  message             #=> Mailflow::Message object
end
```

### Send raw messages

If you already possess a raw RFC2822 formatted message, you can transmit it to our API instead of providing individual fields. You will receive the same result object and exceptions as described earlier.
```ruby
Mailflow.send_raw_message do |m|
  # This is the address under which the message will be recorded as sent.
  m.mail_from "info@address.com"
  # This is an array of addresses that will be the recipients of the message. If you want to blind carbon copy (BCC) 
  # the message to someone, you should include their address here but not in the actual raw message.
  m.rcpt_to "info@address.com", "support@address.com", "hello@address.com", "help@address.com"
  # This is the raw message in the form of a string.
  m.data raw_message
end
```

## Find messages

To retrieve information such as status and details about a message you've sent, you can search for it using its ID. The simplest way to do this is by calling find_by_id.
```ruby
Mailflow::Message.find_by_id(12345)
```


To conserve bandwidth, the API will only provide information requested by the client. By default, it will return only the id of the message and the token. When accessing methods on Mailflow::Message instances, calls will be made to our API automatically to retrieve the necessary information. You can preload this data as demonstrated below:
```ruby
message = Mailflow::Message.includes(:status, :details, :plain_body).find_by_id(12345)
message.status #=> Sent
message.plain_body #=> "Hello There!"
# This will return, but with an additional API call to retrieve the `html_body` expansion.
message.html_body => "<p>Hello There!</p>"
```

You can include any of the following expansions when querying a message. If you anticipate using the data, you can optimize by requesting the necessary expansions before performing the lookup.
* `status` - This provides information regarding the status of an outgoing message.
* `details` - This includes the fundamental details about the message, such as recipient, subject, and so on.
* `inspection` - This includes information related to spam and virus checking.
* `plain_body` - This includes the plain text body of the message.
* `html_body` - This includes the HTML body of the message.
* `attachments` - This includes the attachments, including the data contained within them.
* `headers` - This includes all the headers for the email.
* `raw_message` - This includes the complete RFC2822 message.

## Read message information

Once you have a `Mailflow::Message` object, whether from sending a message or looking it up, you can utilize it to access various properties of the message. The example below demonstrates the methods available to you on the message object.
```ruby
#
# Core Attributes
#
message.id                        # => ID of the message
message.token                     # => The random token associated with this message.

#
# Status Attributes, provided with the 'status'
#
message.status                    # => Status of the message
message.last_delivery_attempt     # => The timestamp of the most recent delivery attempt.
message.held?                     # => Is this message currently on hold?
message.hold_expiry               # => The timestamp indicating when the hold on this message will expire.



#
# Details Attributes, provided with the 'details'
#
message.rcpt_to                   # => The recipient's email address.
message.mail_from                 # => The sender's email address from which the message was sent.
message.subject                   # => The subject line of the message.
message.message_id                # => The ID of the message.
message.timestamp                 # => The timestamp indicating when the message was received by our system.
message.direction                 # => Indicates whether the message is incoming or outgoing.
message.size                      # => The size of the raw message in bytes.
message.bounce?                   # => Is this message categorized as a bounce?
message.bounce_for_id             # => The ID of the message for which this message is categorized as a bounce.
message.tag                       # => The tag associated with the message.
message.received_with_ssl?        # => Was this message received by us using SSL encryption?

#
# Inspection Attributes, provided with the 'inspection' expansion
#
message.inspected?                # => Has this message undergone inspection for spam or threats?
message.spam?                     # => Is this message classified as spam?
message.spam_score                # => Spam score
message.threat?                   # => Is this message flagged as a threat?
message.threat_details            # => The details regarding any detected threats in the message.

#
# Plain Body, provided with the 'plain_body'
#
message.plain_body                # => Plain body

#
# HTML Body, provided with the 'html_body'
#
message.html_body                 # => The HTML body

#
# Attachments, provided with the 'attachments'
#
message.attachments.each do |attachment|
  attachment.filename             # => Name of an attachment.
  attachment.content_type         # => The content type of an attachment in the message.
  attachment.size                 # => The size of an attachment in bytes within the message.
  attachment.hash                 # => SHA1 hash of the attachment's content.
  attachment.data                 # => The raw data of the attachment.
end

#
# Headers, provided with the 'headers'
#
messages.headers['x-someheader']   # => An array of all items for the header x-someheader.
messages.headers.each do |key, values|
  values.each do |value|
    key                           # => The key for the header in lowercase.
    value                         # => Header value.
  end
end

#
# Raw Message, provided with the 'raw_message'
#
message.raw_message               # => The complete RFC2822 formatted message.
```
