require File.expand_path('../lib/mailflow/version', __FILE__)

Gem::Specification.new do |s|
  s.name          = "mailflow"
  s.description   = %q{Mailflow.cloud Ruby library}
  s.summary       = s.description
  s.homepage      = "https://mailflow.cloud"
  s.version       = Mailflow::VERSION
  s.files         = Dir.glob("{lib}/**/*")
  s.require_paths = ["lib"]
  s.authors       = ["Mailflow"]
  s.email         = ["support@mailflow.cloud"]
  s.licenses      = ['MIT']
  s.add_dependency "moonrope-client", ">= 1.0.5", "< 1.1"
end
